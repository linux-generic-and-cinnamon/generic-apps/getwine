#!/usr/bin/env python3
# © Drugwash, 2022 GPL v2 or later
#================================================================
# IMPORTS
#================================================================
import gi
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import Gtk
from gi.repository import Gio
from gi.repository import Gdk, GdkPixbuf
import os, sys, getpass, glob, subprocess, re	# other modules
import urllib.request
import requests
from pprint import pprint as pprint
import locale
dl = locale.getdefaultlocale()
#locale.setlocale(locale.LC_ALL, 'ro_RO.UTF-8')
#locale.setlocale(locale.LC_ALL, '')
import gettext
localedir = "locale"
tr = {}; langs = ['en_US.UTF-8', 'ro_RO.UTF-8']
for l in langs:
	tr[l] = gettext.translation('GetWine', localedir, languages=[l], fallback=True)
tr['.'.join(dl)].install(names=['gettext', 'ngettext'])

try:
	from bs4 import BeautifulSoup as bs
except ModuleNotFoundError as err:
	print(err)
	print("Trying to install missing module, please wait...")
	[id, sts] = os.system('pip install bs4')
	if sts == 0:
		from bs4 import BeautifulSoup as bs
	else:
		raise ImportError('bs4 module cannot be installed')
try:
	from natsort import natsorted
except ModuleNotFoundError as err:
	print(err)
	print("Trying to install missing module, please wait...")
	[id, sts] = os.system('pip install natsort')
	if sts == 0:
		from natsort import natsorted
	else:
		raise ImportError('natsort module cannot be installed')
#================================================================
# COMMAND LINE ARGUMENTS PROCESSING
#================================================================
'''
if len(sys.argv) < 3:
	print("Please provide both Wine version as x.y AND bitness as 32 or 64")
	sys.exit(1)
ver = sys.argv[1]
bits = sys.argv[2]
if bits != "32" and bits != "64":
	print("Please provide bitness as 32 or 64")
	sys.exit(1)
'''
#================================================================
# GLOBAL VARIABLES
#================================================================
debug		= True
ptype		= False
ver			= ""
iver		= ""
bits		= ""
homepath	= "/home/" + getpass.getuser() + "/"		# user home path
storpath	= homepath + "Downloads/Utilities/Wine/"	# main download path
srcpath		= storpath + "src/"							# sources path
debugPath	= storpath + "_work/"
temppath	= homepath + "tmp/wine2pol"					# temp extraction path
destpath	= homepath + ".PlayOnLinux/wine/linux-x86/"	# destination path
winebase	= "https://dl.winehq.org/wine-builds/"
ubase		= winebase + "ubuntu/dists/"				# Ubuntu dists base URL
rexVer		= ".*?Version:\s.*?~.*?$"
rexVerM		= re.compile(r"(?ms)(?:.*Version:\s)(.*)(?:~.*$)")
# add RegEx for Filename: through SHA1:
#================================================================
# TOOLTIPS
#================================================================
ttb1 = _("""Click to select a different path for the Wine packages.
The folder based on version number will be created automatically""")
ttb2 = _("Click to select a different path for the sources package")
ttb3 = _("Click to perform selected action(s)")
ttc1 = _("""Select the distro base type of the target setup
i.e. 'Ubuntu' for Mint or 'Debian' for LMDE""")
ttc2 = _("""Select the distro base version of the target setup
i.e. 'Bionic' for Mint 19.x""")
ttc3 = _("Select the desired Wine type")
ttc4 = _("Select the desired Wine bitness")
ttc5 = _("Select the desired Wine version to download and/or install")
ttc6 = _("Select which version to unpack and install")
ttc7 = _("Select which bitness to unpack and install")
ttc8 = _("Select the desired Wine version to install in PlayOnLinux")
ttk1 = _("""Enable this if you want to download the main packages
(not necessary if they are already downloaded)""")
ttk2 = _("""Enable this if you want to download the debug packages
(not necessary for normal operation)""")
ttk3 = _("""Enable this if you want to download the source package too
(not necessary for normal operation)""")
ttk4 = _("""Enable this if you want to install downloaded or existing
packages selected in the packages path above""")
ttp1 = _("This is the path to the folder where main packages will be downloaded to")
ttp2 = _("This is the path to the folder where sources will be downloaded to")
#================================================================
# STATUSBAR MESSAGES
#================================================================
sbl11 = _("Ready")
sbl12 = _("Busy")
sbl13 = _("Installing")
sbl21 = _("Select desired operation(s)")
sbl22 = _("Please wait...")
sbl23 = _("Finished")
sbl24 = _("Updating {} version list")
sbl25 = _("Downloading pkg")
sbl26 = _("Downloading src")
sbl27 = _("Unpacking...")
#================================================================
# WINDOW GUI
#================================================================
class GetWine(Gtk.ApplicationWindow):

	def __init__(self, app):
		super(GetWine, self).__init__(title="Get Wine", application=app)
		ipath = os.path.dirname(os.path.realpath(__file__))+"/getwine.png"
		if os.path.isfile(ipath):
			self.set_icon(GdkPixbuf.Pixbuf.new_from_file(ipath))
		else:
			print("Error setting own icon")
			self.set_icon_name("folder-download")
		self.grid = Gtk.Grid(halign=Gtk.Align.START, valign=Gtk.Align.CENTER,
			margin = 5, column_spacing = 3, row_spacing = 3)
		# MENU BAR
		menubar = Gtk.MenuBar()
		fmi = Gtk.MenuItem.new_with_label(_("File"))
		menu = Gtk.Menu()
		dmi = Gtk.CheckMenuItem.new_with_label(_("Debug mode"))
		dmi.set_active(debug)
		dmi.connect("toggled", self.debugmode)
		pmi = Gtk.CheckMenuItem.new_with_label(_("Large progressbar"))
		pmi.connect("toggled", self.progtype)
		sep = Gtk.SeparatorMenuItem()
		emi = Gtk.MenuItem.new_with_label(_("Exit"))
		emi.connect("activate", self.quitApp)
		menu.append(dmi)
		menu.append(pmi)
		menu.append(sep)
		menu.append(emi)
		fmi.set_submenu(menu)
		menubar.add(fmi)
		# row 1
		self.label1 = Gtk.Label(label=_("Distro base:"), halign=Gtk.Align.START
			, valign=Gtk.Align.CENTER, margin = 2)
		self.label2 = Gtk.Label(label=_("Codename:"), halign=Gtk.Align.START
			, valign=Gtk.Align.CENTER, margin = 2)
		self.label3 = Gtk.Label(label=_("Type:"), halign=Gtk.Align.START
			, valign=Gtk.Align.CENTER, margin = 2)
		self.label4 = Gtk.Label(label=_("Bitness:"), halign=Gtk.Align.START
			, valign=Gtk.Align.CENTER, margin = 2)
		self.label5 = Gtk.Label(label=_("Version:"), halign=Gtk.Align.START
			, valign=Gtk.Align.CENTER, margin = 2)
		# row 2
		self.cb1 = Gtk.ComboBoxText(margin = 2)
		self.cb1.set_entry_text_column(0)
		self.cb1.connect('changed', self.setDistro)
		self.cb1.set_tooltip_text(ttc1)

		self.cb2 = Gtk.ComboBoxText(margin = 2)
		self.cb2.set_entry_text_column(0)
		self.cb2.connect('changed', self.setBase)
		self.cb2.set_tooltip_text(ttc2)

		self.cb3 = Gtk.ComboBoxText(margin = 2)
		self.cb3.set_entry_text_column(0)
		self.cb3.append_text("stable")
		self.cb3.append_text("staging")
		self.cb3.append_text("devel")
		self.cb3.connect('changed', self.setType)
		self.cb3.set_tooltip_text(ttc3)

		self.cb4 = Gtk.ComboBoxText(margin = 2)
		self.cb4.set_entry_text_column(0)
		self.cb4.append_text(_("32 bit only"))
		self.cb4.append_text(_("64 bit only"))
		self.cb4.append_text(_("32 & 64 bit"))
		self.cb4.connect('changed', self.setBits)
		self.cb4.set_tooltip_text(ttc4)

		self.cb5 = Gtk.ComboBoxText(margin = 2)
		self.cb5.set_entry_text_column(0)
		self.cb5.connect('changed', self.setVer)
		self.cb5.set_tooltip_text(ttc5)
		# row 3
		label6 = Gtk.Label(label=_("Download:"), halign=Gtk.Align.END
			, valign=Gtk.Align.CENTER, margin = 0)
		self.ck1 = Gtk.CheckButton.new_with_label(_("main packages"))
		self.ck1.set_mode(True)		# True=checkbox, False=toggle button
		self.ck1.set_halign(Gtk.Align.START)
		self.ck1.connect('toggled', self.tglDown)
		self.ck1.set_tooltip_text(ttk1)

		self.ck2 = Gtk.CheckButton.new_with_label(_("debug pkgs"))
		self.ck2.set_mode(True)		# True=checkbox, False=toggle button
		self.ck2.set_halign(Gtk.Align.START)
		self.ck2.connect('toggled', self.tglDbg)
		self.ck2.set_tooltip_text(ttk2)

		self.ck3 = Gtk.CheckButton.new_with_label(_("sources"))
		self.ck3.set_mode(True)		# True=checkbox, False=toggle button
		self.ck3.set_halign(Gtk.Align.START)
		self.ck3.connect('toggled', self.tglSrc)
		self.ck3.set_tooltip_text(ttk3)
		# row 4
		label7 = Gtk.Label(label=_("Packages path:"), halign=Gtk.Align.END
			, valign=Gtk.Align.CENTER, margin = 0)
		self.path1 = Gtk.Entry()
		self.path1.set_text(storpath)
		self.path1.connect("preedit-changed", self.txtChg)
		self.path1.set_tooltip_text(ttp1)
		self.btn1 = Gtk.Button(label="...")
		self.btn1.connect("clicked", self.downPath, storpath, self.path1)
		self.btn1.set_tooltip_text(ttb1)
		# row 5
		self.label8 = Gtk.Label(label=_("Sources path:"), halign=Gtk.Align.END
			, valign=Gtk.Align.CENTER, margin = 0)
		self.path2= Gtk.Entry()
		self.path2.set_text(srcpath)
		self.path2.set_tooltip_text(ttp2)
		self.btn2 = Gtk.Button(label="...")
		self.btn2.connect("clicked", self.downPath, srcpath, self.path2)
		self.btn2.set_tooltip_text(ttb2)
		# row 6
		self.ck4 = Gtk.CheckButton.new_with_label(_("Install:"))
		self.ck4.set_mode(True)		# True=checkbox, False=toggle button
		self.ck4.set_halign(Gtk.Align.START)
		self.ck4.connect('toggled', self.tglInst)
		self.ck4.set_tooltip_text(ttk4)

		self.cb6 = Gtk.ComboBoxText(margin = 2)
		self.cb6.set_entry_text_column(0)
		self.cb6.connect('changed', self.setUnpkVer)
		self.cb6.set_tooltip_text(ttc8)

		self.cb7 = Gtk.ComboBoxText(margin = 2)
		self.cb7.set_entry_text_column(0)
		self.cb7.append_text(_("32 bit only"))
		self.cb7.append_text(_("64 bit only"))
		self.cb7.append_text(_("32 & 64 bit"))
		self.cb7.connect('changed', self.setUnpk)
		self.cb7.set_tooltip_text(ttc7)

		self.btn3 = Gtk.Button(label=_("Start"))
		self.btn3.connect("clicked", self.proceed)
		self.btn3.set_tooltip_text(ttb3)
		btn4 = Gtk.Button(label=_("Exit"))
		btn4.connect("clicked", self.quitApp)
		# fake status bar
		sep = Gtk.Separator.new(Gtk.Orientation.HORIZONTAL)
		self.cid1 = Gtk.Label(label=sbl11, halign=Gtk.Align.START
			, valign=Gtk.Align.CENTER, margin = 0)
		self.cid2 = Gtk.Label(label=sbl21, halign=Gtk.Align.START
			, valign=Gtk.Align.CENTER, margin = 0)
		self.pbar1 = Gtk.ProgressBar.new() #min-horizontal-bar-width:30)
		self.pbar1.set_show_text(True)
		self.pbar1.set_fraction(0.0)
		self.pbar1.set_size_request(80, 16)
		self.pbar2 = Gtk.Entry.new()
		self.pbar2.set_editable(False)
		self.pbar2.set_alignment(xalign=0.5)
		self.pbar2.set_text("0%")
		self.pbar2.set_progress_fraction(0.0) # use this only with Gtk.Entry
#		self.pbar2.size_allocate(Gdk.Rectangle(0, 0, 80, 16))
		self.pbar2.set_size_request(80, 16)
		self.pbar = self.pbar2 if ptype else self.pbar1
		# set states (use NORMAL when needed)
		self.label1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.label2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.label3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.label4.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.label5.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.cb1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.cb2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.cb3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.cb4.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.cb5.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
#		self.path1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
#		self.btn1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.label8.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.path2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.cb6.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.cb7.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.btn2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		self.btn3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		# assembly
		self.grid.add(menubar)
		self.grid.attach(self.label1, 0, 1, 1, 1)
		self.grid.attach(self.label2, 1, 1, 1, 1)
		self.grid.attach(self.label3, 2, 1, 1, 1)
		self.grid.attach(self.label4, 3, 1, 1, 1)
		self.grid.attach(self.label5, 4, 1, 1, 1)

		self.grid.attach(self.cb1, 0, 2, 1, 1)
		self.grid.attach(self.cb2, 1, 2, 1, 1)
		self.grid.attach(self.cb3, 2, 2, 1, 1)
		self.grid.attach(self.cb4, 3, 2, 1, 1)
		self.grid.attach(self.cb5, 4, 2, 1, 1)

		self.grid.attach(label6, 0, 3, 1, 1)
		self.grid.attach(self.ck1, 1, 3, 1, 1)
		self.grid.attach(self.ck2, 2, 3, 1, 1)
		self.grid.attach(self.ck3, 3, 3, 1, 1)

		self.grid.attach(label7, 0, 4, 1, 1)
		self.grid.attach(self.path1, 1, 4, 3, 1)
		self.grid.attach(self.btn1, 4, 4, 1, 1)

		self.grid.attach(self.label8, 0, 5, 1, 1)
		self.grid.attach(self.path2, 1, 5, 3, 1)
		self.grid.attach(self.btn2, 4, 5, 1, 1)

		self.grid.attach(self.ck4, 0, 6, 1, 1)
		self.grid.attach(self.cb6, 1, 6, 1, 1)
		self.grid.attach(self.cb7, 2, 6, 1, 1)
		self.grid.attach(self.btn3, 3, 6, 1, 1)
		self.grid.attach(btn4, 4, 6, 1, 1)

		self.grid.attach(sep, 0, 7, 5, 1)

		self.grid.attach(self.cid1, 0, 8, 1, 1)
		self.grid.attach(self.cid2, 1, 8, 2, 1)
		self.grid.attach(self.pbar1, 3, 8, 2, 1)
		self.grid.attach(self.pbar2, 3, 8, 2, 1)
		self.add(self.grid)

		self.isDown = False
		self.isDbg = False
		self.isSrc = False
		self.isInst = False
		self.type = ""
		self.bits = ""
		self.unpk = ""
# we'd better build a dictionary of lists right off the bat
#		self.getData(winebase, self.cb1)
		self.biglist = {}
		self.doneUrls = {}
		for dist in self.getData(winebase, self.cb1):
			d = winebase + dist + "/dists/"
			if debug:
				print(d)
			self.biglist[dist] = self.getData(d)
		if debug:
			print(self.biglist)
		self.getExist(storpath)
		self.show_all()
		self.pbar1.hide()
		self.pbar2.hide()
#================================================================
# TOGGLES
#================================================================
	def tglDown(self, btn):
		self.isDown = btn.get_active()
		if self.isDown == True:
			self.label1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.label2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.label3.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.label4.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.label5.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb3.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb4.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb5.set_state_flags(Gtk.StateFlags.NORMAL, True)
#			self.path1.set_state_flags(Gtk.StateFlags.NORMAL, True)
#			self.btn1.set_state_flags(Gtk.StateFlags.NORMAL, True)
		elif self.isSrc == False:
			self.label1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.label2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.label3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.label4.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.label5.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb4.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb5.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
#			self.path1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
#			self.btn1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			if self.isInst != True:
				self.btn3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)

	def tglDbg(self, btn):
		self.isDbg = btn.get_active()
		'''
		if self.isDbg == True:
			#
		else:
			#
		'''

	def tglSrc(self, btn):
		self.isSrc = btn.get_active()
		if self.isSrc == True:
			self.label8.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.path2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.btn2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.label1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.label2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.label3.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.label4.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.label5.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb3.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb4.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb5.set_state_flags(Gtk.StateFlags.NORMAL, True)
		elif self.isDown == False:
			self.label8.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.path2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.btn2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.label1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.label2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.label3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.label4.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.label5.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb4.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb5.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			# if not all downloads enabled set btn3 as INSENSITIVE

	def tglInst(self, btn):
		self.isInst = btn.get_active()
		if self.isInst == True:
			self.cb6.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.cb7.set_state_flags(Gtk.StateFlags.NORMAL, True)
			if iver != "" and self.cb7.get_active() >= 0:
				self.btn3.set_state_flags(Gtk.StateFlags.NORMAL, True)
		else:
			self.cb6.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.cb7.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			if self.isDown != True:
				self.btn3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
#================================================================
# SELECTIONS
#================================================================
	def setDistro(self, widget):
		dist = widget.get_active_text()
		if dist == "":
			return
		# build page URL according to selected distro
		# many distros have different URL architecture, better lose 'dists/
		self.shortdist = winebase + dist + "/"
		self.dist = winebase + dist + "/dists/"
		self.getData(self.dist, self.cb2)
		self.cb2.set_active(-1)
		self.cb2.set_state_flags(Gtk.StateFlags.NORMAL, True)
		self.label2.set_state_flags(Gtk.StateFlags.NORMAL, True)
		if debug:
			print("dist =", self.dist)

	def setBase(self, widget):
		base = widget.get_active_text()
		if base == "":
			return
		# build page URL according to selected base
		self.base = self.dist + base + "/main/"
# check which types are available for the selected distro and base,
# and rebuild the combo 3 list if necessary
		self.cb3.set_state_flags(Gtk.StateFlags.NORMAL, True)
		self.label3.set_state_flags(Gtk.StateFlags.NORMAL, True)
		if debug:
			print("base =", self.base)

# this one has to be run only when Type and Bitness are both selected
	def chkVer(self):
		if self.type == "" or self.bits == "":
			return
		rex = re.compile(r"(?ms)Package:\swine-" + self.type + rexVer)
# we should save all Packages and Sources files for the session
# in order to avoid duplicated web traffic when user is undecided
# They are in unpacked, gz, and xz form
		self.cid1.set_text(sbl12)
		if self.bits == "32" or self.bits == "both":
			baseurl = self.base + "binary-i386/Packages"
			sbmsg = sbl24.format("32bit")
			if debug:
				print(sbmsg)
				self.page32 = open(debugPath + "Packages", "r").read()
			else:
				self.cid2.set_text(sbmsg)
				self.pbar.show()
				while Gtk.events_pending():
					Gtk.main_iteration()
				self.download2(baseurl, debugPath + "Packages", True, sbmsg)
				self.pbar.hide()
				self.page32 = open(debugPath + "Packages", "r").read()
			vlist32 = self.extractData(self.page32, rex, rexVerM, True)
		if self.bits == "64" or self.bits == "both":
			baseurl = self.base + "binary-amd64/Packages"
			sbmsg = sbl24.format("64bit")
			if debug:
				print(sbmsg)
				self.page64 = open(debugPath + "Packages64", "r").read()
			else:
				self.cid2.set_text(sbmsg)
				self.pbar.show()
				while Gtk.events_pending():
					Gtk.main_iteration()
				self.download2(baseurl, debugPath + "Packages64", True, sbmsg)
				self.pbar.hide()
				self.page64 = open(debugPath + "Packages64", "r").read()
			vlist64 = self.extractData(self.page64, rex, rexVerM, True)
		vlist = vlist32 if self.bits == "32" else vlist64 if self.bits == "64" \
			else natsorted(set(vlist32).intersection(vlist64), reverse=True) \
			if self.bits == "both" else []
		self.cb5.remove_all()
		for i in vlist:
			self.cb5.append_text(i)
		self.cb5.set_state_flags(Gtk.StateFlags.NORMAL, True)
		self.label5.set_state_flags(Gtk.StateFlags.NORMAL, True)
		baseurl = self.base + "source/Sources"
		sbmsg = sbl24.format("sources")
		if debug:
			print(sbmsg)
			self.sources = open(debugPath + "Sources", "r").read()
		else:
			self.cid2.set_text(sbmsg)
			self.pbar.show()
			while Gtk.events_pending():
				Gtk.main_iteration()
			self.download2(baseurl, debugPath + "Sources", True, sbmsg)
			self.pbar.hide()
			self.sources = open(debugPath + "Sources", "r").read()
		self.cid1.set_text(sbl11)
		self.cid2.set_text(sbl23)
		if debug:
			print(sbl23)
		'''
		r = urllib.request.urlopen(baseurl)
		pprint(dir(r))
		len = int(r.headers.get('content-length'))
		print("Sources length =", len)
		self.sources = r.read().decode()
		'''

	def extractData(self, page, rex, rexM, rev=False):
		dataList = []
		soupText = bs(page, 'html.parser').get_text()
		text = rex.findall(soupText)
		if debug:
			print("checking", self.type, "\n", rex)
#			print(text)
		for i in text:
#			r = re.match(r"(?ms)(?:.*Version:\s)(.*)(?:~.*$)", i)
			r = rexM.match(i)
			if r:
				if r.group(1) not in dataList:
					dataList.append(r.group(1))
					print(r.group(1))
		# we could also use
		# verList.append(re.match(r"(?ms)(?:.*Version:\s)(.*)(?:~.*$)", i).group(1))
		# and then return natsorted(set(verList), reverse=True)
		# use sorting here to get a nice list
#		return sorted(verList, reverse=True)
#		print(dataList)
		return natsorted(dataList, reverse=rev)

	def setType(self, widget):
		t = widget.get_active() # get_active_text() doesn't make sense here
		if t == "":
			return
		# build download URLs according to selected type
		self.type = "stable" if t == 0 else "staging" if t == 1 \
			else "devel" if t == 2 else ""
		self.cb4.set_state_flags(Gtk.StateFlags.NORMAL, True)
		self.label4.set_state_flags(Gtk.StateFlags.NORMAL, True)
		self.chkVer()
		if debug:
			print("type =", self.type)

	def setBits(self, widget):
		b = widget.get_active() # get_active_text() doesn't make sense here
		# build download URLs according to selected bitness
		self.bits = "32" if b == 0 else "64" if b == 1 \
			else "both" if b == 2 else ""
		self.cb5.set_state_flags(Gtk.StateFlags.NORMAL, True)
		self.label5.set_state_flags(Gtk.StateFlags.NORMAL, True)
		self.chkVer()
		if debug:
			print("bits = ", self.bits)

	def setVer(self, widget):
		global ver
		ver = widget.get_active_text()
		if ver == "":
			return
		# build download URLs according to selected version
		# self.path1.set_text(storpath + ver + "/")
		self.btn3.set_state_flags(Gtk.StateFlags.NORMAL, True)
		if debug:
			print("version =", ver)

	def setUnpkVer(self, widget):
		global iver
		i = widget.get_active()
		if i == 0 and ver != "" and self.isDown:
			iver = ver
		elif i > 0:
			iver = widget.get_active_text()
		if iver != "" and self.cb7.get_active() >= 0:
			self.btn3.set_state_flags(Gtk.StateFlags.NORMAL, True)
		if debug:
			print("install = v", iver, self.unpk)

	def setUnpk(self, widget):
		b = widget.get_active() # get_active_text() doesn't make sense here
		# prepare for unpacking selected bitness
		self.unpk = "_i386" if b == 0 else "_amd64" if b == 1 else ""
		# check if files exist in packages path?
		if iver != "" and b >= 0:
			self.btn3.set_state_flags(Gtk.StateFlags.NORMAL, True)
		if debug:
			print("install = v", iver, self.unpk)

	def downPath(self, btn, old, lbl):
		dialog = Gtk.FileChooserDialog(_("Select destination path:"), self,
			Gtk.FileChooserAction.SELECT_FOLDER,
			(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,	# "_Cancel"
			Gtk.STOCK_SAVE, Gtk.ResponseType.OK))		# "_Open" or "_Save"
		# self.add_filters(dialog)
		dialog.set_current_folder(old)
		dialog.set_local_only(False)
		dialog.set_show_hidden(True)
		dialog.set_create_folders(True)
		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			path = dialog.get_filename()
			if lbl == self.path1:
				storpath = path
				self.getExist(storpath)
			elif lbl == self.path2:
				srcpath = path
			lbl.set_text(path)
			if debug:
				print("Save clicked,\nPath selected:", path)
		elif response == Gtk.ResponseType.CANCEL:
			if debug:
				print("Cancel clicked")
		dialog.destroy()

	def txtChg(self, widget, txt):
# this is not right, there is no 'ver' when
# installing only or building path manually
		v = widget.get_text()
		if (iver != "" and iver in v and self.isInst) \
			or (ver != "" and ver in v and self.isDown):
			self.btn3.set_state_flags(Gtk.StateFlags.NORMAL, True)
		else:
			self.btn3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
#================================================================
# ACTION
#================================================================
	def getData(self, url, ctrl=None):
		if url in self.doneUrls:
			list = self.doneUrls[url]
			if debug:
				print("list requested for", url, "in control", str(ctrl), "\n", list)
		else:
			list = self.buildList(url, ctrl)
		if ctrl and list != []:
			for i in list:
				ctrl.append_text(i)
		return list

	def buildList(self, url, ctrl):
		if debug:
			xfile = "wine-builds.html" if url == winebase \
				else "wine-builds_ubuntu_dists.html" if url == ubase else ""
			try:
				dists = open(debugPath + xfile, "r").read()
			except:
				print("debug error for", url)
				return []
		else:
			try:
				dists = urllib.request.urlopen(url).read().decode()
			except:
# we have different subdirectory structures for android, fedora, macosx, mageia
# android has all apk versions directly in https://dl.winehq.org/wine-builds/android/
# fedora has numbered subdirs [24 through 36] with i686 and x86_64 > *.rpm
# macosx has tar.gz and pkg in the only pool subdir, and there's also a download.html
# https://dl.winehq.org/wine-builds/macosx/download.html
# mageia has a subdir '6' with i586 and x86_64 > *.rpm
# All these should be analyzed and decided if they're worth adding to the interface
				print("error for", url)
				return []
		soupText = bs(dists, 'html.parser')
		if ctrl:
			ctrl.remove_all()
		list = []
		for item in soupText.find_all('a'):
			d = str(item.string)
			if re.findall("/$", d):
				i = d[:len(d)-1]
				if debug:
					print(i)
#				if ctrl:
#					ctrl.append_text(i)
				list.append(i)
		self.doneUrls[url] = list
		return list

	def getExist(self, path):
		v = []
		for p in glob.glob(path + "*/"):
			v.append(p.split('/')[-2])
		v = natsorted(v, reverse=True)
		if debug:
			print(v)
		self.cb6.remove_all()
		self.cb6.append_text(_("current"))
		for i in v:
			try:
				if float(i):
					self.cb6.append_text(str(i))
			except ValueError:
				pass
#================================================================
	def proceed(self, btn):
		if self.isDown:
			self.cid1.set_text(sbl12)
			self.cid2.set_text(sbl22)
			while Gtk.events_pending():
				Gtk.main_iteration()
			# get all URLs and download files
# this is where we should build the list of files to download
# self.type can be stable/staging/devel
# self.bits can be 32/64/both
# self.isDbg and self.isSrc are boolean
			self.downList = []
# THIS ENTIRE REGEX THING IS SCREWED UP BIG TIME !!!
# process self.page32 and self.page64
			rexD = re.compile(r"(?ms)Version:\s" + re.escape(ver) + "~.*?MD5sum.*?$")
			rexDM = re.compile(r"(?ms)(?:.*Filename:\s)(.*?)(?:$)")
			list32 = self.extractData(self.page32, rexD, rexDM)
			list64 = self.extractData(self.page64, rexD, rexDM)
			print("="*80)
			for i in list32:
				if (self.isDbg == False and "-dbg" in i): #or "winehq" in i:
					continue
				if self.type in i:
					print("adding 32bit pkg:", i.split('/')[-1])
					self.downList.append(i)
			for i in list64:
				if (self.isDbg == False and "-dbg" in i): #or "winehq" in i:
					continue
				if self.type in i:
					print("adding 64bit pkg:", i.split('/')[-1])
					self.downList.append(i)
			print("="*80)
			dest = self.path1.get_text() + ver + "/"
			if not os.path.isdir(dest):
				cmnd = "mkdir -p " + dest
				if debug:
					print(cmnd)
				else:
					subprocess.check_output(cmnd, shell=True)
			if ver != "":
				self.pbar.show()
				t = len(self.downList)
				for i in range(t):
					pkg = self.downList[i]
					sbmsg = sbl25 + " " + str(i+1) + "/" + str(t)
					if debug:
						print(sbmsg)
						print("from\t" + self.shortdist + pkg)
						print("to\t" + dest)
					else:
						self.cid2.set_text(sbmsg)
						self.download2(self.shortdist + pkg, dest + pkg.split('/')[-1], False, sbmsg)
				self.pbar.hide()
# add Sources paths if self.isSrc is True
		if self.isSrc:
			usrc = self.base + "source/"
			self.srcList = []
			rexS = re.compile(r"(?ms)Version:\s" + re.escape(ver) + "~.*?Checksums-Sha1.*?$")
			rexSM = re.compile(r"(?ms)(?:.*Files:\s)(.*?)(?:\nChecksums-Sha1:$)")
			slist = self.extractData(self.sources, rexS, rexSM)
			for i in slist:
				if self.type in i or "staging" not in i:
					block = i.split('\n')
					for line in block:
						elem = line.split(' ')
						self.srcList.append(elem[-1])
						print(elem[-1], elem[-2] + " bytes")
			print("="*80)
			self.pbar.show()
			t = len(self.srcList)
			for i in range(t):
				pkg = self.srcList[i]
				sbmsg = sbl26 + " " + str(i+1) + "/" + str(t)
				if debug:
					print(sbmsg)
					print("from\t" + self.base + "source/" + pkg)
					print("to\t" + srcpath)
				else:
					self.cid2.set_text(sbmsg)
					self.download2(self.base + "source/" + pkg, srcpath + pkg.split('/')[-1], False, sbmsg)
			self.pbar.hide()
		# and/or just install existing packages
		if self.isInst:
			self.cid1.set_text(sbl13)
			self.cid2.set_text(sbl22)
			while Gtk.events_pending():
				Gtk.main_iteration()
			self.dothejob(True)
		self.cid1.set_text(sbl11)
		self.cid2.set_text(sbl23)

#	def download2(self, url, path, ovw=False):
	def download2(self, url, path, ovw=False, msg=""):
		if os.path.isfile(path):
			fsz = os.path.getsize(path)
#		if ovw == False and os.path.isfile(path) == True:
		if ovw == False and fsz > 0:
			print("File exists; skipping:", path)
			return False
		cnk = 8192 # chunk size [8 kB]
		ttl = 0 # total downloaded
		fstream = None
		try:
			fstream = urllib.request.urlopen(url)
		except:
			print("Error opening", url)
		else:
			# alternatively fstream.getheaders() or fstream.headers[]
			flen = int(fstream.headers.get('content-length'))
			dlen = self.fmtsize2(flen)
			msg = msg + " (" + dlen + ")" if flen != 0 else msg
			self.cid2.set_text(msg)
			tfile = open(path,'wb') # 'wb' is write binary
			while True:
				buf = fstream.read(cnk)
				if len(buf) == 0:
					break
				tfile.write(buf)
				ttl += cnk
				f = float(ttl) / float(flen)
				if not ptype:
					self.pbar.set_fraction(f)
				else:
					self.pbar.set_text(str(int(f*100)) + "%")
					self.pbar.set_progress_fraction(f)
				while Gtk.events_pending():
					Gtk.main_iteration()
			tfile.close()
			del tfile
		finally:
			if fstream is not None:
				fstream.close()
		if not ptype:
			self.pbar.set_fraction(0.0)
		else:
			self.pbar.set_text("0%")
			self.pbar.set_progress_fraction(0.0)
		return True
	'''
	# from https://wiki.ubuntu.com/Quickly/Snippets
	# make MD5 hash
import hashlib
		f = open(filepath)
		digest = hashlib.md5()
		while True:
			buf = f.read(4096)
			if buf == "":
				break
			digest.update(buf)
		f.close()
		print digest.hexdigest()
	'''
#================================================================
	'''
	def fmtsize(self, f, unit='m'):
		fsz = f if isinstance(f, int) else os.path.getsize(f)
		exp = {'b': 0, 'k': 1, 'm': 2, 'g': 3, 't': 4}
		if unit not in exp:
			raise ValueError("Select unit from b/k/m/g/t")
		else:
			size = fsz / 1024 ** exp[unit]
			return round(size, 1)
	'''
#================================================================
	def fmtsize2(self, f):
		fsz = f if isinstance(f, int) else os.path.getsize(f)
		n = 0
		exp = {0: 'B', 1: 'kB', 2: 'MB', 3: 'GB', 4: 'TB'}
		while fsz > 1024:
			fsz /= 1024
			n += 1
		return str(round(fsz, 1)) + " " + exp[n]
#================================================================
	def debugmode(self, item):
		global debug
		debug = not debug
		print("debug=", debug)

	def progtype(self, mode):
		global ptype
		ptype = not ptype
		vis = self.pbar.is_visible()
		self.pbar.hide()
		self.pbar = self.pbar1 if not ptype else self.pbar2
# if progressbar was visible then show it
		if vis:
			self.pbar.show()

	def dothejob(self, item):
		if iver == "":
			return
		if not os.path.isdir(temppath):
			cmnd = "mkdir -p " + temppath
			if debug:
				print(cmnd)
			else:
				subprocess.check_output(cmnd, shell=True)
		t = self.getType(temppath + "/opt/*")
		if t and os.path.isdir(temppath + "/opt/" + t):
			self.cleanup(temppath + "/opt")
			#self.opendeb(self.path1.get_text(), bits)
		self.cid2.set_text(sbl27)
		while Gtk.events_pending():
			Gtk.main_iteration()
		self.opendeb(self.path1.get_text() + iver + "/")
		t = self.getType(temppath + "/opt/*")
		self.movefiles(temppath + "/opt/" + t, destpath)
		self.cleanup(temppath)

	def getType(self, path):
		try:
			p = glob.glob(temppath + "/opt/*")
			for i in p:
				f = i.split('/')[-1]
				print("/opt/" + f)
			return f
		except:
			return ""

	#def opendeb(self, path, bits):
	def opendeb(self, path):
		#pre = "_i386" if bits == "32" else "_amd64" if bits == "64" else ""
		#for pkg in glob.glob(path + "*" + pre + ".deb"):
		# This is where we need to actually make an ordered list
		# of packages for installation according to bitness,
		# as installing in the wrong order can botch the setup !!!
		for pkg in glob.glob(path + "*" + self.unpk + ".deb"):
			cmnd = "dpkg-deb -x " + pkg + " " + temppath + "/"
			if debug:
				print(cmnd)
			else:
				subprocess.check_output(cmnd, shell=True)
		if debug:
			print("Finished extracting Wine " + self.unpk + " deb packages")

	def movefiles(self, src, dest):
		fold = src.split("/")
		#if debug:
		#	print(fold)
		f = fold[len(fold) - 1]  # new folder name
		#if debug:
		#	print("f=" + f)
		newsrc = src.replace(f, iver)
		cmnd = "mv " + src + " " + newsrc
		if debug:
			print(cmnd)
		else:
			subprocess.check_output(cmnd, shell=True)
		cmnd = "mv -t " + dest + " " + newsrc
		if debug:
			print(cmnd)
		else:
			subprocess.check_output(cmnd, shell=True)
		if debug:
			print("Finished moving folders to destination:\n" +
				src + " to " + destpath + iver)

	def cleanup(self, fld):
		cmnd = "rm -r " + fld
		if debug:
			print(cmnd)
		else:
			subprocess.check_output(cmnd, shell=True)
		if debug:
			print("Finished cleanup on path " + fld)

	def quitApp(self, par):
		app.quit()
#================================================================
class Application(Gtk.Application):
	def __init__(self):
		super(Application, self).__init__()

	def do_activate(self):
		self.win = GetWine(self)
		self.win.pbar.show()
		wm, wn = self.win.get_preferred_size()
		self.win.resize(wm.width, wm.height)
		self.win.pbar.hide()
		if debug:
			print("window size = ", wm.width, " x ", wm.height)
		hints = Gdk.Geometry()
		hints.min_width = wm.width
		hints.max_width = wm.width
		hints.min_height = wm.height
		hints.max_height = wm.height
		mask = Gdk.WindowHints.MIN_SIZE | Gdk.WindowHints.MAX_SIZE # 11 | 12
		self.win.set_geometry_hints(None, hints, mask)

	def do_startup(self):
		Gtk.Application.do_startup(self)

app = Application()
app.run()
